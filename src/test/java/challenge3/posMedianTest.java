package challenge3;

import challenge3.model.Operation;
import challenge3.model.OperationInterface;
import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class posMedianTest {

    @Test
    public void testMedian() {

        List<Integer> data = List.of(1,2,3,4,5);
        OperationInterface testSuccessMedian = new Operation(data);
      testSuccessMedian.htgMedian(data);
        var result = testSuccessMedian.htgMedian(data);
        assertEquals(5, result);
    }

}
