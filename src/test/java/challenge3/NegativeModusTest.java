package challenge3;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;

import java.util.List;

public class NegativeModusTest {


    public int htgMode(List<Integer> m) {
        int n = m.size();
        int maxValue = 0, maxCount = 0, i, j;
        for (i = 0; i < n; ++i) {
            int count = 0;
            for (j = 0; j < n; ++j) {
                if (m.get(j) == m.get(i))
                    ++count;
            }
            if (count > maxCount) {
                maxCount = count;
                maxValue = m.get(i);
            }
        }
        if (maxCount != 7) {
            throw new IllegalArgumentException("Mode salah");
        }else {
            return maxValue;
        }
    }

    @Test
    @DisplayName("Negative Test count modus")
    void negativeModusTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            List<Integer> data = List.of(7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10,
                    7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 5, 6, 7,
                    7, 7, 7, 7,	7, 7, 7, 8,	8, 8, 8, 8,	9, 9, 9, 9,	9, 9, 10, 10, 10, 10, 10,
                    7, 7, 7, 7,	7, 7, 7, 8,	8, 8, 8, 8,	9, 9, 9, 9,	9, 9, 10, 10, 10, 10, 10, 7, 7,	10,
                    7, 7, 7, 7,	7, 7, 7, 8,	8, 8, 8, 8,	9, 9, 9, 9,	9, 9, 10, 10, 10, 10, 10,
                    7,	7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10,
                    7, 7, 7, 7,	7,	7, 7, 8, 8,	8,	8,	8,	9,	9,	9,	9,	9,	9, 10, 10, 10, 10, 10, 6, 7, 8,
                    7, 7, 7, 7,	7,	7,	7,	8,	8,	8,	8,	8,	9,	9,	9,	9,	9,	9,	10,	10,	10,	10,	10,	7,	7,	9,	9,	8
            );

        Assertions.assertEquals("Mode salah", htgMode(data));
        });
    }
}
