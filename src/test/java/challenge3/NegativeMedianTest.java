package challenge3;

import challenge3.model.Operation;
import challenge3.model.OperationInterface;
import org.junit.jupiter.api.*;
import java.util.List;

public class NegativeMedianTest {

    /**
     * Implementasi unit testing untuk negative test case.
     *
     */
    @BeforeAll
    public static void BeforeAll(){
        System.out.println("Sebelum Test");
    }
    @AfterAll
    public static void AfterAll(){
        System.out.println("Setelah Test");
    }

    @Test
    @DisplayName("Negative Tes Hitung Median Invalid Input")
    public void HtgMedianTest(){
        Exception e = Assertions.assertThrows(IllegalArgumentException.class,() ->{
        List<Integer> data= List.of();
        OperationInterface Operation = new Operation(data);
        Operation.htgMedian(data);
    });
}
}
