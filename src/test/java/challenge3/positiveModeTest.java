package challenge3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

public class positiveModeTest {
    public int htgMode(List<Integer> m) {
        int n = m.size();
        int maxValue = 0, maxCount = 0, i, j;
        for (i = 0; i < n; ++i) {
            int count = 0;
            for (j = 0; j < n; ++j) {
                if (m.get(j) == m.get(i))
                    ++count;
            }
            if (count > maxCount) {
                maxCount = count;
                maxValue = m.get(i);
            }
        }
        return maxValue;
    }
    @Test
      void positiveModeTest(){
        List<Integer> data = List.of(1,1,2,3);
        assertEquals(1,htgMode(data));
    }
}
