package challenge3;

import static org.junit.jupiter.api.Assertions.*;
import challenge3.model.Operation;
import challenge3.model.OperationInterface;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.List;


public class PositivMeanTest {

    public byte htgMean(List<Integer> m) {
        double sum = 0;
        for (int i = 0; i < m.size(); i++) {
            sum += m.get(i);
        }
        if(sum == 0){
            throw new IllegalArgumentException("tidak boleh nol");
        } else {
            double rata = sum / m.size();
            return (byte) rata;
        }
//        return 0;
    }

    @Test
    void PostesMeanSuccess(){
        List<Integer> data = List.of(1,2,3,4,5);
        assertEquals(3 , htgMean(data));

    }

}
