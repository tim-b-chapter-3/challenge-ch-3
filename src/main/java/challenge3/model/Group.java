package challenge3.model;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Implementasi GroupInterface
 */

public class Group implements GroupInterface {

    /**
     * Implementasi HashMap dari Java Collection
     */
    private HashMap<Integer, Integer> grupLebihDari = new HashMap<>();
    private HashMap<Integer, Integer> grupKurangDari = new HashMap<>();
    private HashMap<Integer, Integer> grupTengah = new HashMap<>();

    // Constructor Group
    public Group(List<Integer> data, int pembatas) {
        this.setNilaiLebihDari(data, pembatas);
        this.setNilaiKurangDari(data, pembatas);
        this.setNilaiTengah(data, pembatas);
    }
    // Getter Method
    public HashMap<Integer, Integer> getNilaiLebihDari() {
        return grupLebihDari;
    }

    public HashMap<Integer, Integer> getNilaiKurangDari() {
        return grupKurangDari;
    }

    public HashMap<Integer, Integer> getNilaiTengah() {
        return grupTengah;
    }
    // Setter Method
    public void setNilaiLebihDari(List<Integer> data, int pembatas) {
        HashMap<Integer, Integer> map = new HashMap<>();

        for (Integer nilai : data) {
            int count = 0;
            if (nilai > pembatas) {
                for (Integer nilai2 : data) {
                    if (Objects.equals(nilai2, nilai)) {
                        ++count;
                    }
                }
                this.grupLebihDari.put(nilai, count);
            }
        }
    }

    public void setNilaiKurangDari(List<Integer> data, int pembatas) {
        HashMap<Integer, Integer> map = new HashMap<>();

        for (Integer nilai : data) {
            int count = 0;
            if (nilai < pembatas) {
                for (Integer nilai2 : data) {
                    if (Objects.equals(nilai2, nilai)) {
                        ++count;
                    }
                }
                this.grupKurangDari.put(nilai, count);
            }
        }
    }

    public void setNilaiTengah(List<Integer> data, int pembatas) {
        HashMap<Integer, Integer> map = new HashMap<>();

        for (Integer nilai : data) {
            int count = 0;
            if (nilai == pembatas) {
                for (Integer nilai2 : data) {
                    if (Objects.equals(nilai2, nilai)) {
                        ++count;
                    }
                }
                this.grupTengah.put(nilai, count);
            }
        }
    }
}
