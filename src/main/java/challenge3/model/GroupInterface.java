package challenge3.model;

import java.util.List;

public interface GroupInterface {

    void setNilaiLebihDari(List<Integer> data, int delimiter);

    void setNilaiKurangDari(List<Integer> data, int delimiter);

    void setNilaiTengah(List<Integer> data, int delimiter);

}
