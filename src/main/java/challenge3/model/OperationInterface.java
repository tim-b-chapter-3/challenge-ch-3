package challenge3.model;

import java.util.List;


// 


public interface OperationInterface {


    void htgMode(List<Integer> m);

    byte htgMean(List<Integer> m);

    double htgMedian(List<Integer> m);

}

