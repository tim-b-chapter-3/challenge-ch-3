package challenge3.model;

import java.util.List;

/**
 * Implentasi OperationInterface
 */
public class Operation implements OperationInterface {

    private double mean;
    private double median;
    private int mode;

    // Constructor Operation
    public Operation(List<Integer> data) {
        this.htgMean(data);
        this.htgMedian(data);
        this.htgMode(data);
    }

    // Getter Method
    public double getMean() {
        return mean;
    }

    public int getMode() {
        return mode;
    }

    public double getMedian() {
        return median;
    }

    // Setter Method

    /**
     * Positif = Viona
     * Negatif = Rasyid
     * @return
     */
    public byte htgMean(List<Integer> m) {
        double sum = 0;
        for (int i = 0; i < m.size(); i++) {
            sum += m.get(i);
        }
        if(sum == 0){
            throw new IllegalArgumentException("tidak boleh nol");
        } else {
        double rata = sum / m.size();
        this.mean = rata;
        }
        return 0;
    }


    /**
     * Positif = Billy
     * Negatif = Aris
     */
    public void htgMode(List<Integer> m) {
        int n = m.size();
        int maxValue = 0, maxCount = 0, i, j;
        for (i = 0; i < n; ++i) {
            int count = 0;
            for (j = 0; j < n; ++j) {
                if (m.get(j) == m.get(i))
                    ++count;
            }
            if (count > maxCount) {
                maxCount = count;
                maxValue = m.get(i);
            }
        }
        this.mode = maxValue;
    }


    /**
     * Positif = Hasnul
     * Negatif = Yohanes
     *
     * @return
     */
    public double htgMedian(List<Integer> m) {
        List<Integer> urut = m.stream().sorted().toList();
        double nTengah;
       if ( urut.size() == 0)
            throw new IllegalArgumentException("Maaf data belum tersedia");
        if (urut.size() % 2 == 1)
            nTengah = urut.get(urut.size() / 2);
        else
            nTengah = ((double) (urut.get(urut.size() / 2) + urut.get(urut.size() / 2-1))) / 2;
        this.median = nTengah;
        return nTengah;
    }

}