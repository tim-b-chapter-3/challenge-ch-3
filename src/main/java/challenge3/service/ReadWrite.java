package challenge3.service;

import challenge3.model.Group;
import challenge3.model.Operation;
import java.io.*;
import java.util.*;
import static java.util.Collections.sort;

/**
 * Implementasi Interface dari ReadWriteInterface Class
 */
public class ReadWrite implements ReadWriteInterafce{

    public final List<Integer> Data_Nilai = new ArrayList<>();
    private final String HASIL = "FinalFile";
    FailedMenu menu = new FailedMenu();

    // Constructor method
    ReadWrite() {
        File createDirectory = new File(HASIL);
        if (createDirectory.mkdir()) {
            System.out.println("Folder" + HASIL + "berhasil di cetak");
        }
/**
 * Implentasi List dari java collection
 */
        String fileName = "data_sekolah.csv";
        List<List<String>> data = this.readFIle(fileName, ";");
        for (List<String> dataTiapKelas : data) {
            dataTiapKelas.remove(0);
            for (int j = 0; j < dataTiapKelas.size(); j++) {
                Data_Nilai.add(Integer.valueOf(dataTiapKelas.get(j)));
            }
        }
       sort(Data_Nilai, Integer::compareTo);
    }

    /**
     * validasi dan exception ketika file tidak tergenerate
     *
     */
    public void generateOperation(String txtFile){
        Operation oper = new Operation(Data_Nilai);
        try {
            File file = new File(HASIL + "/" + txtFile);
            if (file.createNewFile()){
                System.out.println("New file is created");
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Hasil Perhitungan Data: ");
            bwr.newLine();
            bwr.newLine();
            bwr.write("Nilai Mean\t\t: " + oper.getMean());
            bwr.newLine();
            bwr.write("Nilai Median\t: " + oper.getMedian());
            bwr.newLine();
            bwr.write("Nilai Mode\t\t: " + oper.getMode());
            bwr.newLine();
            bwr.flush();
            bwr.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void generateGroup(String txtFile, int delimiter){
        Group grup = new Group(Data_Nilai, delimiter);
        try {
            File file = new File(HASIL + "/" + txtFile);
            if (file.createNewFile()){
                System.out.println("New file is created");
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Hasil Pengelompokan Data: ");
            bwr.newLine();
            bwr.newLine();

            HashMap<Integer, Integer> kurang = new HashMap<>(grup.getNilaiKurangDari());
            for (Integer nilai2 : kurang.keySet()) {
                bwr.write("Nilai " + nilai2 + " berjumlah " + kurang.get(nilai2));
                bwr.newLine();
            }
            bwr.newLine();

            HashMap<Integer, Integer> tengah = new HashMap<>(grup.getNilaiTengah());
            for (Integer nilai3 : tengah.keySet()) {
                bwr.write("Nilai " + nilai3 + " berjumlah " + tengah.get(nilai3));
                bwr.newLine();
            }
            bwr.newLine();

            HashMap<Integer, Integer> jumlah = new HashMap<>(grup.getNilaiLebihDari());
            for (Integer nilai : jumlah.keySet()) {
                bwr.write("Nilai " + nilai + " berjumlah " + jumlah.get(nilai));
                bwr.newLine();
            }
            bwr.newLine();
            bwr.newLine();
            bwr.flush();
            bwr.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Implementasi Optional Class
     */

    public List<List<String>> readFIle(String fileName, String delimiter){
        Optional<List<List<String>>> data = Optional.empty();
        try {
            File file = new File(fileName);
            if (file.exists()) {
                Scanner scanner = new Scanner(file);
                List<List<String>> data2 = new ArrayList<>();
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    List<String> arr = new ArrayList<>();
                    String[] delim = line.split(delimiter);
                    for (String data5 : delim) {
                        arr.add(data5);
                    }
                    data2.add(arr);
                }
                data = Optional.of(data2);
            } else {
                System.out.println("File " + fileName + " tidak ditemukan");
            }
        } catch (FileNotFoundException e) {
            menu.tampil();
            return null;
        }
        return data.get();
    }

}
//  public List<List<String>> readFIle(String fileName, String delimiter){


//        try{
//            O
//            File file = new File(fileName);
//            FileReader fr = new FileReader(file);
//            BufferedReader br = new BufferedReader(fr);
//            String line;
//            String[] tempArr;
//            List<String>innerList;
//            List<List<String>> outerList = new ArrayList<>();
//            while ((line=br.readLine())!=null){
//                tempArr = line.split(delimiter);
//                innerList = new ArrayList<>(Arrays.asList(tempArr));
//                outerList.add(innerList);
//            }
//            br.close();
//            return outerList;
//        }catch (Exception e){
//           menu.tampil();
//            return null;
//        }
// }
